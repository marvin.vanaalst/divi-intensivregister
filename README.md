# Setup

- Install [miniconda](https://conda.io/en/latest/miniconda.html)

- Create the conda environment, activate it and start the notebook

```
conda env create -f environment.yml
conda activate rki
jupyter notebook divi-intensivregister.ipynb
```